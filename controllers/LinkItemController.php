<?php

namespace sycms\controllers;

use Yii;
use sycms\models\LinkItem;
use sycms\models\search\LinkItemSearch;
use sybase\SybaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;


/**
 * LinkItemController implements the CRUD actions for LinkItem model.
 */
class LinkItemController extends SybaseController
{
    public function behaviors()
    {
        return array_merge(
            $this->getAccess(),
            [                
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['post'],
                    ],
                ],
            ]);
    }

    /**
     * Lists all LinkItem models.
     * @return mixed
     */
    public function actionIndex($cid)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = 'ajax';
        }
        
        $searchModel = new LinkItemSearch();
        $searchModel->link_category_id = $cid;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LinkItem model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = 'ajax';
        }
        
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LinkItem model.
     * If creation is successful, the browser will be redirected to the 'update' page.
     * @return mixed
     */
    public function actionCreate($save = null, $cid)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = 'ajax';
        }
        
        $render = 'create';
        
        $model = new LinkItem();    
        $model->link_category_id = $cid;

        if ($model->load(Yii::$app->request->post())) {
        
            if (empty($save)) {
                
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                
                $linkItemLast = LinkItem::find()
                        ->andWhere(['link_category_id' => $model->link_category_id])
                        ->orderBy('order DESC')
                        ->asArray()->one();
                
                $model->order = $linkItemLast['order'] + 1;
            
                if ($model->save()) {
                
                    Yii::$app->session->setFlash('status', 'success');
                    Yii::$app->session->setFlash('message1', 'Tambah Data Sukses');
                    Yii::$app->session->setFlash('message2', 'Proses tambah data sukses. Data telah berhasil disimpan.');

                    $render = 'update';
                } else {
                
                    $model->setIsNewRecord(true);

                    Yii::$app->session->setFlash('status', 'danger');
                    Yii::$app->session->setFlash('message1', 'Tambah Data Gagal');
                    Yii::$app->session->setFlash('message2', 'Proses tambah data gagal. Data gagal disimpan.');                
                }
            }
        }
        
        return $this->render($render, [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LinkItem model.
     * If update is successful, the browser will be redirected to the 'update' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $save = null)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = 'ajax';
        }
        
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
        
            if (empty($save)) {
                
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                
                $pageId = !empty($model->page_id) ? $model->page_id : null;
                $articleCategoryId = !empty($model->article_category_id) ? $model->article_category_id : null;
                $articleItemId = !empty($model->article_item_id) ? $model->article_item_id : null;
                $url = !empty($model->url) ? $model->url : null;
                $anchor = !empty($model->anchor) ? $model->anchor : null;
                
                $model->page_id = null;
                $model->article_category_id = null;
                $model->article_item_id = null;
                $model->url = null;
                $model->anchor = null;
                
                if ($model->type == 'Page') {
                    
                    $model->page_id = $pageId;
                } else if ($model->type == 'Article-Category') {
                    
                    $model->article_category_id = $articleCategoryId;
                } else if ($model->type == 'Article-Item') {
                    
                    $model->article_item_id = $articleItemId;
                } else if ($model->type == 'Url') {
                    
                    $model->url = $url;
                } else if ($model->type == 'Anchor') {
                    
                    $model->anchor = $anchor;
                } 
            
                if ($model->save()) {
                
                    Yii::$app->session->setFlash('status', 'success');
                    Yii::$app->session->setFlash('message1', 'Update Sukses');
                    Yii::$app->session->setFlash('message2', 'Proses update sukses. Data telah berhasil disimpan.');
                } else {
                
                    Yii::$app->session->setFlash('status', 'danger');
                    Yii::$app->session->setFlash('message1', 'Update Gagal');
                    Yii::$app->session->setFlash('message2', 'Proses update gagal. Data gagal disimpan.');
                }
            }
        }
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LinkItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (($model = $this->findModel($id)) !== false) {
                        
            $flag = false;
            $error = '';
            
            try {
                $flag = $model->delete();
            } catch (yii\db\Exception $exc) {
                $error = Yii::$app->params['errMysql'][$exc->errorInfo[1]];
            }
        }
        
        if ($flag) {
        
            Yii::$app->session->setFlash('status', 'success');
            Yii::$app->session->setFlash('message1', 'Delete Sukses');
            Yii::$app->session->setFlash('message2', 'Proses delete sukses. Data telah berhasil dihapus.');            
        } else {
        
            Yii::$app->session->setFlash('status', 'danger');
            Yii::$app->session->setFlash('message1', 'Delete Gagal');
            Yii::$app->session->setFlash('message2', 'Proses delete gagal. Data gagal dihapus.' . $error);
        }

        $return = [];
        
        $return['url'] = Yii::$app->urlManager->createUrl([Yii::$app->params['sycmsPath'] . '/link-item/index', 'cid' => $model->link_category_id]);
        
        Yii::$app->response->format = Response::FORMAT_JSON;                        
        return $return;
    }
    
    public function actionUp($id) {
        
        $model = $this->findModel($id);
        
        if ($model->order > 1) {
            
            $transaction = Yii::$app->db->beginTransaction();
            $flag = false;
            
            $modelTemp = LinkItem::findOne(['order' => $model->order - 1]);
            $modelTemp->order = $model->order;
            
            $model->order = 0;
            
            if (($flag = $model->save())) {       
                
                if (($flag = $modelTemp->save())) {
                    
                    $model->order = $modelTemp->order - 1;

                    $flag = $model->save();
                }
            }
            
            if ($flag) {
                $transaction->commit();
            } else {
                
                Yii::$app->session->setFlash('status', 'danger');
                Yii::$app->session->setFlash('message1', 'Update Gagal');
                Yii::$app->session->setFlash('message2', 'Proses update urutan gagal.');
                
                $transaction->rollBack();
            }
        }
        
        return $this->runAction('index', ['cid' => $model->link_category_id]);
    }
    
    public function actionDown($id) {
        
        $model = $this->findModel($id);
        
        $modelTemp = LinkItem::findOne(['order' => $model->order + 1]);
        
        if ($modelTemp !== null) {
            
            $transaction = Yii::$app->db->beginTransaction();
            $flag = false;
            
            $modelTemp->order = $model->order;

            $model->order = 0;
            
            if (($flag = $model->save())) {  
                
                if (($flag = $modelTemp->save())) {
                    
                    $model->order = $modelTemp->order + 1;

                    $flag = $model->save();
                }
            }
            
            if ($flag) {
                $transaction->commit();
            } else {
                
                Yii::$app->session->setFlash('status', 'danger');
                Yii::$app->session->setFlash('message1', 'Update Gagal');
                Yii::$app->session->setFlash('message2', 'Proses update urutan gagal.');
                
                $transaction->rollBack();
            }
        }
        
        return $this->runAction('index', ['cid' => $model->link_category_id]);
    }

    /**
     * Finds the LinkItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LinkItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LinkItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
