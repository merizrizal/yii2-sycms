<?php

namespace sycms\controllers;

use Yii;
use sycms\models\ArticleItem;
use sycms\models\search\ArticleItemSearch;
use sycms\models\ArticleImage;
use sycomponent\Tools;
use sybase\SybaseController;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;


/**
 * ArticleItemController implements the CRUD actions for ArticleItem model.
 */
class ArticleItemController extends SybaseController
{
    public function behaviors()
    {
        return array_merge(
            $this->getAccess(),
            [                
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['post'],
                    ],
                ],
            ]);
    }

    /**
     * Lists all ArticleItem models.
     * @return mixed
     */
    public function actionIndex($cid)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = 'ajax';
        }
        
        $searchModel = new ArticleItemSearch();
        $searchModel->article_category_id = $cid;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ArticleItem model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = 'ajax';
        }
        
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ArticleItem model.
     * If creation is successful, the browser will be redirected to the 'update' page.
     * @return mixed
     */
    public function actionCreate($save = null, $cid)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = 'ajax';
        }
        
        $render = 'create';
        
        $model = new ArticleItem();
        $model->article_category_id = $cid;

        if ($model->load(Yii::$app->request->post())) {
        
            if (empty($save)) {
                
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                
                $model->slug = StringHelper::truncate(Inflector::slug($model->title), 64, '');
                
                $model->image = Tools::uploadFile('/img/article_item/', $model, 'image', 'slug');
            
                if ($model->save()) {
                
                    Yii::$app->session->setFlash('status', 'success');
                    Yii::$app->session->setFlash('message1', 'Tambah Data Sukses');
                    Yii::$app->session->setFlash('message2', 'Proses tambah data sukses. Data telah berhasil disimpan.');

                    $render = 'update';
                } else {
                
                    $model->setIsNewRecord(true);

                    Yii::$app->session->setFlash('status', 'danger');
                    Yii::$app->session->setFlash('message1', 'Tambah Data Gagal');
                    Yii::$app->session->setFlash('message2', 'Proses tambah data gagal. Data gagal disimpan.');                
                }                                
            }
        }
        
        return $this->render($render, [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ArticleItem model.
     * If update is successful, the browser will be redirected to the 'update' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $save = null)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = 'ajax';
        }
        
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
        
            if (empty($save)) {
                
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                
                $model->slug = StringHelper::truncate(Inflector::slug($model->title), 64, '');
                
                if (($model->image = Tools::uploadFile('/img/article_item/', $model, 'image', 'slug'))) {
                
                } else {
                    $model->image = $model->oldAttributes['image'];
                }
            
                if ($model->save()) {
                
                    Yii::$app->session->setFlash('status', 'success');
                    Yii::$app->session->setFlash('message1', 'Update Sukses');
                    Yii::$app->session->setFlash('message2', 'Proses update sukses. Data telah berhasil disimpan.');
                } else {
                
                    Yii::$app->session->setFlash('status', 'danger');
                    Yii::$app->session->setFlash('message1', 'Update Gagal');
                    Yii::$app->session->setFlash('message2', 'Proses update gagal. Data gagal disimpan.');
                }
            }
        }
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ArticleItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (($model = $this->findModel($id)) !== false) {
            
            $flag = false;
            $error = '';
            
            try {
                $flag = $model->delete();
            } catch (yii\db\Exception $exc) {
                $error = Yii::$app->params['errMysql'][$exc->errorInfo[1]];
            }
        }
        
        if ($flag) {
        
            Yii::$app->session->setFlash('status', 'success');
            Yii::$app->session->setFlash('message1', 'Delete Sukses');
            Yii::$app->session->setFlash('message2', 'Proses delete sukses. Data telah berhasil dihapus.');            
        } else {
        
            Yii::$app->session->setFlash('status', 'danger');
            Yii::$app->session->setFlash('message1', 'Delete Gagal');
            Yii::$app->session->setFlash('message2', 'Proses delete gagal. Data gagal dihapus.' . $error);
        }

        $return = [];
        
        $return['url'] = Yii::$app->urlManager->createUrl([Yii::$app->params['sycmsPath'] . '/article-item/index', 'cid' => $model->article_category_id]);
        
        Yii::$app->response->format = Response::FORMAT_JSON;                        
        return $return;
    }
    
    public function actionImage($id) {

        if (Yii::$app->request->isAjax) {
            $this->layout = 'ajax';
        }

        $model = $this->findModel($id);

        return $this->render('image', [
            'model' => $model,
            'modelArticleImage' => new ArticleImage(),
        ]);
    }

    public function actionSaveImage($id) {

        if (!empty($post = Yii::$app->request->post())) {

            $transaction = Yii::$app->db->beginTransaction();
            $flag = false;

            $modelArticleImage = new ArticleImage();
            $modelArticleImage->load($post);

            $modelArticleImage->article_item_id = $id;
            $modelArticleImage->image = Tools::uploadFile('/img/article_image/', $modelArticleImage, 'image', 'article_item_id');

            $flag = $modelArticleImage->save();

            if ($flag) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
            }
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return [];
    }

    public function actionDeleteImage() {

        $flag = false;

        if (!empty($post = Yii::$app->request->post())) {

            $flag = ArticleImage::findOne($post['id'])->delete();
        }

        $return = [];

        if ($flag) {
            $return['success'] = true;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $return;
    }

    /**
     * Finds the ArticleItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ArticleItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ArticleItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
