<?php

namespace sycms\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use sycms\models\LinkItem;

/**
 * LinkItemSearch represents the model behind the search form about `sycms\models\LinkItem`.
 */
class LinkItemSearch extends LinkItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'link_category_id', 'parent_id', 'page_id', 'article_category_id', 'article_item_id', 'not_active', 'order', 'user_created', 'user_updated'], 'integer'],
            [['title', 'type', 'url', 'created_at', 'updated_at',
                'parent.title'], 'safe'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributes() {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['parent.title']);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LinkItem::find()
                ->joinWith([
                    'parent' => function($query) {
                        $query->from('link_item parent');
                    },
                    'linkCategory',
                ])
                ->andWhere(['link_item.link_category_id' => $this->link_category_id])
                ->orderBy('link_item.order ASC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
            'pagination' => array(
                'pageSize' => 15,
            ),
        ]);
        
//        $dataProvider->sort->attributes['parent.title'] = [
//            'asc' => ['parent.title' => SORT_ASC],
//            'desc' => ['parent.title' => SORT_DESC],
//        ];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'link_item.id' => $this->id,
            'link_item.link_category_id' => $this->link_category_id,
            'link_item.parent_id' => $this->parent_id,
            'link_item.page_id' => $this->page_id,
            'link_item.article_category_id' => $this->article_category_id,
            'link_item.article_item_id' => $this->article_item_id,
            'link_item.not_active' => $this->not_active,
            'link_item.order' => $this->order,
            'link_item.created_at' => $this->created_at,
            'link_item.user_created' => $this->user_created,
            'link_item.updated_at' => $this->updated_at,
            'link_item.user_updated' => $this->user_updated,
        ]);

        $query->andFilterWhere(['like', 'link_item.title', $this->title])
            ->andFilterWhere(['like', 'link_item.type', $this->type])
            ->andFilterWhere(['like', 'link_item.url', $this->url])
            ->andFilterWhere(['like', 'parent.title', $this->getAttribute('parent.title')]);

        return $dataProvider;
    }
}
