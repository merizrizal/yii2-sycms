<?php

namespace sycms\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use sycms\models\Component;

/**
 * ComponentSearch represents the model behind the search form about `sycms\models\Component`.
 */
class ComponentSearch extends Component
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'not_publish', 'user_created', 'user_updated'], 'integer'],
            [['title', 'subtitle', 'image', 'text', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Component::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => array(
                'pageSize' => 15,
            ),
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'not_publish' => $this->not_publish,
            'created_at' => $this->created_at,
            'user_created' => $this->user_created,
            'updated_at' => $this->updated_at,
            'user_updated' => $this->user_updated,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'subtitle', $this->subtitle])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }
}
