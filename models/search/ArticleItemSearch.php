<?php

namespace sycms\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use sycms\models\ArticleItem;

/**
 * ArticleItemSearch represents the model behind the search form about `sycms\models\ArticleItem`.
 */
class ArticleItemSearch extends ArticleItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'article_category_id', 'not_publish', 'user_created', 'user_updated'], 'integer'],
            [['title', 'subtitle', 'image', 'short_text', 'text', 'slug', 'keywords', 'description', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ArticleItem::find()
                ->joinWith([
                    'articleCategory'
                ])
                ->andWhere(['article_item.article_category_id' => $this->article_category_id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => array(
                'pageSize' => 15,
            ),
        ]);

        if (!($this->load($params) && $this->validate())) {            
            return $dataProvider;
        }        

        $query->andFilterWhere([
            'article_item.id' => $this->id,
            'article_item.article_category_id' => $this->article_category_id,
            'article_item.not_publish' => $this->not_publish,
            'article_item.created_at' => $this->created_at,
            'article_item.user_created' => $this->user_created,
            'article_item.updated_at' => $this->updated_at,
            'article_item.user_updated' => $this->user_updated,
        ]);

        $query->andFilterWhere(['like', 'article_item.title', $this->title])
            ->andFilterWhere(['like', 'article_item.subtitle', $this->subtitle])
            ->andFilterWhere(['like', 'article_item.image', $this->image])
            ->andFilterWhere(['like', 'article_item.short_text', $this->short_text])
            ->andFilterWhere(['like', 'article_item.text', $this->text])
            ->andFilterWhere(['like', 'article_item.slug', $this->slug])
            ->andFilterWhere(['like', 'article_item.keywords', $this->keywords])
            ->andFilterWhere(['like', 'article_item.description', $this->description]);

        return $dataProvider;
    }
}
