<?php

namespace sycms\models;

use Yii;
use syuserman\models\User;

/**
 * This is the model class for table "link_item".
 *
 * @property integer $id
 * @property integer $link_category_id
 * @property integer $parent_id
 * @property string $title
 * @property string $type
 * @property integer $page_id
 * @property integer $article_category_id
 * @property integer $article_item_id
 * @property string $url
 * @property string $anchor
 * @property integer $not_active
 * @property integer $order
 * @property string $created_at
 * @property integer $user_created
 * @property string $updated_at
 * @property integer $user_updated
 *
 * @property User $userCreated
 * @property User $userUpdated
 * @property Page $page
 * @property ArticleCategory $articleCategory
 * @property ArticleItem $articleItem
 * @property LinkItem $parent
 * @property LinkItem[] $linkItems
 * @property LinkCategory $linkCategory
 */
class LinkItem extends \sybase\SybaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'link_item';
    }
        

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['link_category_id', 'parent_id', 'page_id', 'article_category_id', 'article_item_id', 'not_active', 'order', 'user_created', 'user_updated'], 'integer'],
            [['title', 'type'], 'required'],
            [['type'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 64],
            [['url'], 'string', 'max' => 128],
            [['anchor'], 'string', 'max' => 32],
            [['user_created'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_created' => 'id']],
            [['user_updated'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_updated' => 'id']],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => Page::className(), 'targetAttribute' => ['page_id' => 'id']],
            [['article_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArticleCategory::className(), 'targetAttribute' => ['article_category_id' => 'id']],
            [['article_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArticleItem::className(), 'targetAttribute' => ['article_item_id' => 'id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => LinkItem::className(), 'targetAttribute' => ['parent_id' => 'id']],
            [['link_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => LinkCategory::className(), 'targetAttribute' => ['link_category_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link_category_id' => 'Kategori Tautan',
            'parent_id' => 'Parent',
            'title' => 'Judul',
            'type' => 'Tipe',
            'page_id' => 'Halaman',
            'article_category_id' => 'Kategori Artikel',
            'article_item_id' => 'Artikel',
            'url' => 'URL',
            'anchor' => 'Anchor',
            'not_active' => 'Not Active',
            'order' => 'Order',
            'created_at' => 'Created At',
            'user_created' => 'User Created',
            'updated_at' => 'Updated At',
            'user_updated' => 'User Updated',
            
            'parent.title' => 'Parent',
            'page.title' => 'Halaman',
            'articleCategory.title' => 'Kategori Artikel',
            'articleItem.title' => 'Artikel',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'user_created']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'user_updated']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleCategory()
    {
        return $this->hasOne(ArticleCategory::className(), ['id' => 'article_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleItem()
    {
        return $this->hasOne(ArticleItem::className(), ['id' => 'article_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(LinkItem::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLinkItems()
    {
        return $this->hasMany(LinkItem::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLinkCategory()
    {
        return $this->hasOne(LinkCategory::className(), ['id' => 'link_category_id']);
    }
}
