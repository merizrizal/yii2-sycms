<?php

namespace sycms\models;

use Yii;
use syuserman\models\User;

/**
 * This is the model class for table "component".
 *
 * @property string $id
 * @property string $title
 * @property string $subtitle
 * @property string $image
 * @property string $text
 * @property string $slug
 * @property integer $not_publish
 * @property string $created_at
 * @property integer $user_created
 * @property string $updated_at
 * @property integer $user_updated
 *
 * @property User $userCreated
 * @property User $userUpdated
 */
class Component extends \sybase\SybaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'component';
    }
        

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'slug'], 'required'],
            [['text'], 'string'],
            [['not_publish', 'user_created', 'user_updated'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'subtitle', 'image', 'slug'], 'string', 'max' => 128],
            [['slug'], 'unique'],
            [['user_created'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_created' => 'id']],
            [['user_updated'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_updated' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Judul',
            'subtitle' => 'Sub Judul',
            'image' => 'Image',
            'text' => 'Text',
            'slug' => 'Slug',
            'not_publish' => 'Not Publish',
            'created_at' => 'Created At',
            'user_created' => 'User Created',
            'updated_at' => 'Updated At',
            'user_updated' => 'User Updated',
        ];
    }
    
    public static function getComponent($params, $islike = false, $isone = false) {
        
        $component = Component::find();
                
        if ($islike) {
            $component = $component->andWhere(['LIKE', 'component.slug', $params]);
        } else {
            $component = $component->andWhere(['component.slug' => $params]);
        }                
        
        if ($isone) {
            $component = $component->asArray()->one();
        } else {
            $component = $component->asArray()->all();
        }
        
        return $component;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'user_created']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'user_updated']);
    }
}
