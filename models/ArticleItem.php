<?php

namespace sycms\models;

use Yii;
use syuserman\models\User;

/**
 * This is the model class for table "article_item".
 *
 * @property integer $id
 * @property integer $article_category_id
 * @property string $title
 * @property string $subtitle
 * @property string $image
 * @property string $short_text
 * @property string $text
 * @property string $slug
 * @property integer $not_publish
 * @property string $keywords
 * @property string $description
 * @property string $created_at
 * @property integer $user_created
 * @property string $updated_at
 * @property integer $user_updated
 *
 * @property ArticleImage[] $articleImages
 * @property ArticleCategory $articleCategory
 * @property User $userCreated
 * @property User $userUpdated
 * @property LinkItem[] $linkItems
 */
class ArticleItem extends \sybase\SybaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_item';
    }
        

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_category_id', 'title', 'short_text', 'text', 'slug'], 'required'],
            [['article_category_id', 'not_publish', 'user_created', 'user_updated'], 'integer'],
            [['short_text', 'text', 'keywords', 'description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'subtitle', 'image', 'slug'], 'string', 'max' => 128],
            [['slug'], 'unique'],
            [['article_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArticleCategory::className(), 'targetAttribute' => ['article_category_id' => 'id']],
            [['user_created'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_created' => 'id']],
            [['user_updated'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_updated' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article_category_id' => 'Article Category ID',
            'title' => 'Judul',
            'subtitle' => 'Sub Judul',
            'image' => 'Image',
            'short_text' => 'Text Singkat',
            'text' => 'Text',
            'slug' => 'Slug',
            'not_publish' => 'Not Publish',
            'keywords' => 'Kata Kunci',
            'description' => 'Deskripsi',
            'created_at' => 'Created At',
            'user_created' => 'User Created',
            'updated_at' => 'Updated At',
            'user_updated' => 'User Updated',
        ];
    }
    
    public static function getItemByCategory($categorySlug, $limit = 4) {
        
        return ArticleItem::find()
                ->joinWith([
                    'articleCategory',
                ])
                ->andWhere(['article_category.slug' => $categorySlug])
                ->andWhere(['article_item.not_publish' => 0])
                ->limit($limit)
                ->orderBy('article_item.id DESC')
                ->asArray()->all();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleImages()
    {
        return $this->hasMany(ArticleImage::className(), ['article_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleCategory()
    {
        return $this->hasOne(ArticleCategory::className(), ['id' => 'article_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'user_created']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'user_updated']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLinkItems()
    {
        return $this->hasMany(LinkItem::className(), ['article_item_id' => 'id']);
    }
}
