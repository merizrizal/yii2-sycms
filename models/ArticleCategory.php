<?php

namespace sycms\models;

use Yii;
use syuserman\models\User;

/**
 * This is the model class for table "article_category".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $title
 * @property string $subtitle
 * @property string $image
 * @property string $slug
 * @property string $keywords
 * @property string $description
 * @property string $created_at
 * @property integer $user_created
 * @property string $updated_at
 * @property integer $user_updated
 *
 * @property ArticleCategory $parent
 * @property ArticleCategory[] $articleCategories
 * @property User $userCreated
 * @property User $userUpdated
 * @property ArticleItem[] $articleItems
 * @property LinkItem[] $linkItems
 */
class ArticleCategory extends \sybase\SybaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_category';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'user_created', 'user_updated'], 'integer'],
            [['title', 'slug'], 'required'],
            [['keywords', 'description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'subtitle', 'image', 'slug'], 'string', 'max' => 128],
            [['slug'], 'unique'],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArticleCategory::className(), 'targetAttribute' => ['parent_id' => 'id']],
            [['user_created'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_created' => 'id']],
            [['user_updated'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_updated' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent',
            'title' => 'Judul',
            'subtitle' => 'Sub Judul',
            'image' => 'Image',
            'slug' => 'Slug',
            'keywords' => 'Kata Kunci',
            'description' => 'Deskripsi',
            'created_at' => 'Created At',
            'user_created' => 'User Created',
            'updated_at' => 'Updated At',
            'user_updated' => 'User Updated',

            'parent.title' => 'Parent',
        ];
    }
    
    public static function getCategoryItem($categorySlug) {
        
        return ArticleCategory::find()
                ->joinWith([
                    'articleItems' => function($query) {
                        $query->andOnCondition(['article_item.not_publish' => 0]);
                    },
                    'articleItems.articleImages'
                ])
                ->andWhere(['article_category.slug' => $categorySlug])
                ->asArray()->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(ArticleCategory::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleCategories()
    {
        return $this->hasMany(ArticleCategory::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'user_created']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'user_updated']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleItems()
    {
        return $this->hasMany(ArticleItem::className(), ['article_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLinkItems()
    {
        return $this->hasMany(LinkItem::className(), ['article_category_id' => 'id']);
    }
}
