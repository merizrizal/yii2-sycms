<?php

namespace sycms\models;

use Yii;
use syuserman\models\User;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property string $title
 * @property string $subtitle
 * @property string $slug
 * @property string $image
 * @property string $text
 * @property string $view_file
 * @property integer $not_publish
 * @property string $keywords
 * @property string $description
 * @property string $created_at
 * @property integer $user_created
 * @property string $updated_at
 * @property integer $user_updated
 *
 * @property LinkItem[] $linkItems
 * @property User $userCreated
 * @property User $userUpdated
 */
class Page extends \sybase\SybaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }
        

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'slug'], 'required'],
            [['text', 'keywords', 'description'], 'string'],
            [['not_publish', 'user_created', 'user_updated'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'subtitle', 'slug', 'image', 'view_file'], 'string', 'max' => 128],
            [['slug'], 'unique'],
            [['user_created'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_created' => 'id']],
            [['user_updated'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_updated' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Judul',
            'subtitle' => 'Sub Judul',
            'slug' => 'Slug',
            'image' => 'Image',
            'text' => 'Text',
            'view_file' => 'View File',
            'not_publish' => 'Not Publish',
            'keywords' => 'Kata Kunci',
            'description' => 'Deskripsi',
            'created_at' => 'Created At',
            'user_created' => 'User Created',
            'updated_at' => 'Updated At',
            'user_updated' => 'User Updated',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLinkItems()
    {
        return $this->hasMany(LinkItem::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'user_created']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'user_updated']);
    }
}
