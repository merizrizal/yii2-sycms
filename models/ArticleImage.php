<?php

namespace sycms\models;

use Yii;
use syuserman\models\User;

/**
 * This is the model class for table "article_image".
 *
 * @property integer $id
 * @property integer $article_item_id
 * @property string $title
 * @property string $image
 * @property string $created_at
 * @property integer $user_created
 * @property string $updated_at
 * @property integer $user_updated
 *
 * @property ArticleItem $articleItem
 * @property User $userCreated
 * @property User $userUpdated
 */
class ArticleImage extends \sybase\SybaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_image';
    }
        

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_item_id'], 'required'],
            [['article_item_id', 'user_created', 'user_updated'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'image'], 'string', 'max' => 128],
            [['article_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArticleItem::className(), 'targetAttribute' => ['article_item_id' => 'id']],
            [['user_created'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_created' => 'id']],
            [['user_updated'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_updated' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article_item_id' => 'Article Item ID',
            'title' => 'Title',
            'image' => 'Image',
            'created_at' => 'Created At',
            'user_created' => 'User Created',
            'updated_at' => 'Updated At',
            'user_updated' => 'User Updated',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleItem()
    {
        return $this->hasOne(ArticleItem::className(), ['id' => 'article_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'user_created']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'user_updated']);
    }
}
