<?php

namespace sycms\models;

use Yii;
use syuserman\models\User;

/**
 * This is the model class for table "link_category".
 *
 * @property integer $id
 * @property string $title
 * @property string $created_at
 * @property integer $user_created
 * @property string $updated_at
 * @property integer $user_updated
 *
 * @property LinkItem[] $linkItems
 */
class LinkCategory extends \sybase\SybaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'link_category';
    }
        

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_created', 'user_updated'], 'integer'],
            [['title'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Judul',
            'created_at' => 'Created At',
            'user_created' => 'User Created',
            'updated_at' => 'Updated At',
            'user_updated' => 'User Updated',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLinkItems()
    {
        return $this->hasMany(LinkItem::className(), ['link_category_id' => 'id']);
    }
}
