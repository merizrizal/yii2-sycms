<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\DetailView;
use sycomponent\AjaxRequest;
use sycomponent\ModalDialog;

/* @var $this yii\web\View */
/* @var $model sycms\models\ArticleCategory */

$ajaxRequest = new AjaxRequest([
    'modelClass' => 'ArticleCategory',
]);

$ajaxRequest->view();

$this->title = StringHelper::truncateWords($model->title, 3);
$this->params['breadcrumbs'][] = ['label' => 'Kategori Artikel', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title; ?>

<?= $ajaxRequest->component() ?>

<div class="article-category-view">
    
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <div class="x_panel">
                
                <div class="x_content">
                    
                    <?= Html::a('<i class="fa fa-pencil"></i>&nbsp;&nbsp;&nbsp;' . 'Edit', 
                        ['update', 'id' => $model->id], 
                        [
                            'class' => 'btn btn-primary',
                            'style' => 'color:white'
                        ]) ?>

                    <?= Html::a('<i class="fa fa-trash"></i>&nbsp;&nbsp;&nbsp;' . 'Delete', 
                        ['delete', 'id' => $model->id], 
                        [
                            'id' => 'delete',
                            'class' => 'btn btn-danger',
                            'style' => 'color:white',
                            'data-not-ajax' => 1,
                            'model-id' => $model->id,
                            'model-name' => $model->title,
                        ]) ?>                            

                    <?= Html::a('<i class="fa fa-rotate-left"></i>&nbsp;&nbsp;&nbsp;' . 'Cancel', 
                        ['index'], 
                        [
                            'class' => 'btn btn-default',
                        ]) ?>
                            
                    <div class="clearfix" style="margin-top: 15px"></div>
                
                    <?= DetailView::widget([
                        'model' => $model,
                        'options' => [
                            'class' => 'table'
                        ],
                        'attributes' => [
                            'parent.title',
                            'title',
                            'subtitle',
                            [
                                'attribute' => 'image',
                                'format' => 'raw',
                                'value' => Html::img(Yii::getAlias('@uploadsUrl') . $model->thumb('/img/article_category/', 'image', 200, 200), ['class'=>'img-thumbnail file-preview-image']),
                            ],
                            'slug',
                            [
                                'label' => '<br><h4><label class="form-label">SEO</label></h4>',
                                'format' => 'raw',
                                'value' => '&nbsp;',
                            ],
                            'keywords:ntext',
                            'description:ntext',
                        ],
                    ]) ?>
                        
                </div>
                                
            </div>
        </div>
    </div>

</div>

<?php
    
$modalDialog = new ModalDialog([
    'clickedComponent' => 'a#delete',
    'modelAttributeId' => 'model-id',
    'modelAttributeName' => 'model-name',
]);

$modalDialog->theScript();

echo $modalDialog->renderDialog();
    
?>