<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model sycms\models\ArticleCategory */

$this->title = 'Update Kategori Artikel: ' . ' ' . StringHelper::truncateWords($model->title, 3);
$this->params['breadcrumbs'][] = ['label' => 'Kategori Artikel', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => StringHelper::truncateWords($model->title, 3), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="article-category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
