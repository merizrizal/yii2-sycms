<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sycms\models\ArticleCategory */

$this->title = 'Create Kategori Artikel';
$this->params['breadcrumbs'][] = ['label' => 'Kategori Artikel', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-category-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
