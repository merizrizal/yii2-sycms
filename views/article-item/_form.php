<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use kartik\file\FileInput;
use sycomponent\AjaxRequest;
use sycomponent\NotificationDialog;

/* @var $this yii\web\View */
/* @var $model sycms\models\ArticleItem */
/* @var $form yii\widgets\ActiveForm */

$ajaxRequest = new AjaxRequest([
    'modelClass' => 'ArticleItem',
]);

$ajaxRequest->form();

$status = Yii::$app->session->getFlash('status');
$message1 = Yii::$app->session->getFlash('message1');
$message2 = Yii::$app->session->getFlash('message2');

if ($status !== null) : 
    $notif = new NotificationDialog([
        'status' => $status,
        'message1' => $message1,
        'message2' => $message2,
    ]);

    $notif->theScript();
    echo $notif->renderDialog();

endif; ?>

<?= $ajaxRequest->component() ?>

<div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-8">
        <div class="x_panel">
            <div class="article-item-form">

                <?php $form = ActiveForm::begin([
                        'id' => 'article-item-form',
                        'action' => $model->isNewRecord ? ['create', 'cid' => $model->article_category_id] : ['update', 'id' => $model->id],
                        'options' => [

                        ],
                        'fieldConfig' => [
                            'parts' => [
                                '{inputClass}' => 'col-lg-12'
                            ],
                            'template' => '<div class="row">'
                                            . '<div class="col-lg-3">'
                                                . '{label}'
                                            . '</div>'
                                            . '<div class="col-lg-6">'
                                                . '<div class="{inputClass}">'
                                                    . '{input}'
                                                . '</div>'
                                            . '</div>'
                                            . '<div class="col-lg-3">'
                                                . '{error}'
                                            . '</div>'
                                        . '</div>', 
                        ]
                ]); ?>
                    
                    <div class="x_title">
                    
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6">
                                    <?php
                                    if (!$model->isNewRecord) {
                                        echo Html::a('<i class="fa fa-upload"></i>&nbsp;&nbsp;&nbsp;' . 'Create', ['create', 'cid' => $model->article_category_id], ['class' => 'btn btn-success']);
                                        echo Html::a('<i class="fa fa-file-image-o"></i>&nbsp;&nbsp;&nbsp;' . 'Gambar', ['image', 'id' => $model->id], ['class' => 'btn btn-success']); 
                                    } ?>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                
                    <div class="x_content">

                        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'subtitle')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'image')->widget(FileInput::classname(), [
                            'options' => [
                                'accept' => 'image/*'
                            ],
                            'pluginOptions' => [
                                'initialPreview' => [
                                    Html::img(Yii::getAlias('@uploadsUrl') . $model->thumb('/img/article_item/', 'image', 200, 200), ['class'=>'file-preview-image']),
                                ],
                                'showRemove' => false,
                                'showUpload' => false,
                            ]
                        ]); ?>

                        <?= $form->field($model, 'short_text', [                        
                            'template' => '<div class="row">
                                                <div class="col-lg-3">
                                                    {label}
                                                </div>
                                                <div class="col-lg-9">
                                                    <div class="{inputClass}">
                                                        {input}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    {error}
                                                </div>
                                            </div>',
                        ])->textarea(['rows' => 6]) ?>

                        <?= $form->field($model, 'text', [                        
                            'template' => '<div class="row">
                                                <div class="col-lg-3">
                                                    {label}
                                                </div>
                                                <div class="col-lg-9">
                                                    <div class="{inputClass}">
                                                        {input}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    {error}
                                                </div>
                                            </div>',
                        ])->widget(CKEditor::className(), [
                            'options' => ['rows' => 6],
                            'preset' => 'full',
                            'clientOptions' => [
                                'extraPlugins' => 'codesnippet,iframe',
                                'allowedContent' => true,
                            ],
                        ]) ?>

                        <?= $form->field($model, 'not_publish')->checkbox(['value' => true], false) ?>
                        
                        <?php
                        if (!$model->isNewRecord) {                            
                            echo $form->field($model, 'slug')->textInput(['maxlength' => true]);
                        } ?>
                        
                        <br>
                        <h4><label class="form-label">SEO</label></h4>

                        <?= $form->field($model, 'keywords')->textarea(['rows' => 3]) ?>

                        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-6">
                                    <?php
                                    $icon = '<i class="fa fa-floppy-o"></i>&nbsp;&nbsp;&nbsp;';
                                    echo Html::submitButton($model->isNewRecord ? $icon . 'Save' : $icon . 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
                                    echo '&nbsp;&nbsp;&nbsp;';
                                    echo Html::a('<i class="fa fa-rotate-left"></i>&nbsp;&nbsp;&nbsp;Cancel', ['index', 'cid' => $model->article_category_id], ['class' => 'btn btn-default']); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
    <div class="col-sm-2"></div>
</div><!-- /.row -->

<?php

$this->registerCssFile($this->params['assetCommon']->baseUrl . '/plugins/iCheck/all.css', ['depends' => 'yii\web\YiiAsset']);
 
$this->registerJsFile($this->params['assetCommon']->baseUrl . '/plugins/iCheck/icheck.min.js', ['depends' => 'yii\web\YiiAsset']);

$jscript = '
    
';

$this->registerJs($jscript . Yii::$app->params['checkbox-radio-script']()); ?>