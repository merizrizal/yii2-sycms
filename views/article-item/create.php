<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sycms\models\ArticleItem */

$this->title = 'Create Artikel ' . $model->articleCategory->title;
$this->params['breadcrumbs'][] = ['label' => 'Kategori Artikel', 'url' => ['article-category/index']];
$this->params['breadcrumbs'][] = ['label' => $model->articleCategory->title, 'url' => ['index', 'cid' => $model->article_category_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-item-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
