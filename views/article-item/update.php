<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model sycms\models\ArticleItem */

$this->title = 'Update Artikel: ' . ' ' . StringHelper::truncateWords($model->title, 3);
$this->params['breadcrumbs'][] = ['label' => 'Kategori Artikel', 'url' => ['article-category/index']];
$this->params['breadcrumbs'][] = ['label' => $model->articleCategory->title, 'url' => ['index', 'cid' => $model->article_category_id]];
$this->params['breadcrumbs'][] = ['label' => StringHelper::truncateWords($model->title, 3), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="article-item-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
