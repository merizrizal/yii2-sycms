<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use sycomponent\AjaxRequest;
use sycomponent\NotificationDialog;

/* @var $this yii\web\View */
/* @var $model sycms\models\Product */

$ajaxRequest = new AjaxRequest([
    'modelClass' => 'ArticleItem',
]);

$ajaxRequest->form();

$status = Yii::$app->session->getFlash('status');
$message1 = Yii::$app->session->getFlash('message1');
$message2 = Yii::$app->session->getFlash('message2');

if ($status !== null) : 
    $notif = new NotificationDialog([
        'status' => $status,
        'message1' => $message1,
        'message2' => $message2,
    ]);

    $notif->theScript();
    echo $notif->renderDialog();

endif;

$this->title = 'Galeri Artikel: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Artikel', 'url' => ['index', 'cid' => $model->article_category_id]];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['update', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Galeri';
?>

<?= $ajaxRequest->component() ?>

<div class="article-image">

    <?php $form = ActiveForm::begin([
        'id' => 'article-item-form',
        'action' => ['image', 'id' => $model->id],
        'options' => [

        ],
    ]); ?>
    
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <div class="x_panel">                    
                    <div class="x_content">
                        <div class="row">
                            
                            <?php
                            if (!empty($model->articleImages)):

                                foreach ($model->articleImages as $articleImage): ?>

                                    <div class="col-sm-3 text-center" style="margin-bottom: 20px">
                                        <?= Html::img(Yii::getAlias('@uploadsUrl') . $articleImage->thumb('/img/article_image/', 'image', 250, 250), ['class'=>'img-thumbnail file-preview-image']) ?>
                                        <div class="clearfix" style="margin-top: 5px"></div>
                                        <?= Html::a('<i class="fa fa-trash"></i> Delete', ['article-item/delete-image'], ['class' => 'btn btn-danger btn-xs delete-image']) ?>
                                        <?= Html::hiddenInput('article-item_image-id', $articleImage['id'], ['class' => 'article-item_image-id']) ?>
                                    </div>


                                <?php
                                endforeach;
                            endif; ?>
                                
                        </div>
                            
                        <div class="clearfix" style="margin-top: 20px"></div>

                        <div class="article-item-form">

                            <?= $form->field($modelArticleImage, 'image')->widget(FileInput::classname(), [
                                'options' => [
                                    'accept' => 'image/*',
                                    'multiple' => true,
                                ],
                                'pluginOptions' => [
                                    'showRemove' => true,
                                    'showUpload' => true,
                                    'uploadUrl' => Url::to(['article-item/save-image', 'id' => $model->id]),
                                ]
                            ]); ?>

                            <div class="form-group">                            
                                <?= Html::a('<i class="fa fa-rotate-left"></i>&nbsp;&nbsp;&nbsp;Cancel', ['update', 'id' => $model->id], ['class' => 'btn btn-default']); ?>                                
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>            
    
    <?php ActiveForm::end(); ?>

</div>

<?php
$jscript = '
    $(".delete-image").on("click", function() {
    
        var thisObj = $(this);
    
        $.ajax({
            cache: false,
            type: "POST",
            data: {
                "id": thisObj.parent().find(".article-item_image-id").val()
            },
            url: thisObj.attr("href"),
            beforeSend: function(xhr) {
                $(".overlay").show();
                $(".loading-img").show();
            },
            success: function(response) {                

                if (response.success) {
                    
                    thisObj.parent().fadeOut(180, function() {
                        $(this).remove();
                    });
                }
                
                $(".overlay").hide();
                $(".loading-img").hide();                
            },
            error: function (xhr, ajaxOptions, thrownError) {                     
                console.log(xhr);

                $(".overlay").hide();
                $(".loading-img").hide();

                $(".error-overlay").find(".error-number").html(xhr.status);
                $(".error-overlay").find(".error-message").html(xhr.responseText);
                $(".error-overlay").show();

                setTimeout(function() {                    
                    $(".error-overlay").hide();
                }, 1000);
            }
        });

        return false;
    });
';

$this->registerJs($jscript); ?>