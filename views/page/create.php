<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sycms\models\Page */

$this->title = 'Create Halaman';
$this->params['breadcrumbs'][] = ['label' => 'Halaman', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
