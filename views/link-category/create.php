<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sycms\models\LinkCategory */

$this->title = 'Create Kategori Tautan';
$this->params['breadcrumbs'][] = ['label' => 'Kategori Tautan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="link-category-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
