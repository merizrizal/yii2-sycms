<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sycms\models\LinkCategory */

$this->title = 'Update Kategori Tautan: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Kategori Tautan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="link-category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
