<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use sycomponent\AjaxRequest;
use sycomponent\ModalDialog;

/* @var $this yii\web\View */
/* @var $model sycms\models\LinkItem */

$ajaxRequest = new AjaxRequest([
    'modelClass' => 'LinkItem',
]);

$ajaxRequest->view();

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Kategori Tautan', 'url' => ['link-category/index']];
$this->params['breadcrumbs'][] = ['label' => $model->linkCategory->title, 'url' => ['index', 'cid' => $model->link_category_id]];
$this->params['breadcrumbs'][] = $this->title; ?>

<?= $ajaxRequest->component() ?>

<div class="link-item-view">
    
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <div class="x_panel">
                
                <div class="x_content">
                    
                    <?= Html::a('<i class="fa fa-pencil"></i>&nbsp;&nbsp;&nbsp;' . 'Edit', 
                        ['update', 'id' => $model->id], 
                        [
                            'class' => 'btn btn-primary',
                            'style' => 'color:white'
                        ]) ?>

                    <?= Html::a('<i class="fa fa-trash"></i>&nbsp;&nbsp;&nbsp;' . 'Delete', 
                        ['delete', 'id' => $model->id], 
                        [
                            'id' => 'delete',
                            'class' => 'btn btn-danger',
                            'style' => 'color:white',
                            'data-not-ajax' => 1,
                            'model-id' => $model->id,
                            'model-name' => $model->title,
                        ]) ?>                            

                    <?= Html::a('<i class="fa fa-rotate-left"></i>&nbsp;&nbsp;&nbsp;' . 'Cancel', 
                        ['index', 'cid' => $model->link_category_id], 
                        [
                            'class' => 'btn btn-default',
                        ]) ?>
                            
                    <div class="clearfix" style="margin-top: 15px"></div>
                
                    <?= DetailView::widget([
                        'model' => $model,
                        'options' => [
                            'class' => 'table'
                        ],
                        'attributes' => [
                            'parent.title',
                            'title',
                            'type',
                            'page.title',
                            'articleCategory.title',
                            'articleItem.title',
                            'url:url',
                            [
                                'attribute' => 'not_active',
                                'format' => 'raw',
                                'value' => Html::checkbox('not_active[]', $model->not_active, ['value' => $model->not_active, 'disabled' => 'disabled']),
                            ],
                        ],
                    ]) ?>
                        
                </div>
                                
            </div>
        </div>
    </div>

</div>

<?php
    
$modalDialog = new ModalDialog([
    'clickedComponent' => 'a#delete',
    'modelAttributeId' => 'model-id',
    'modelAttributeName' => 'model-name',
]);

$modalDialog->theScript();

echo $modalDialog->renderDialog();

$this->registerCssFile($this->params['assetCommon']->baseUrl . '/plugins/iCheck/all.css', ['depends' => 'yii\web\YiiAsset']);

$this->registerJsFile($this->params['assetCommon']->baseUrl . '/plugins/iCheck/icheck.min.js', ['depends' => 'yii\web\YiiAsset']);

$jscript = Yii::$app->params['checkbox-radio-script']()
    . '$(".iCheck-helper").parent().removeClass("disabled");
';

$this->registerJs($jscript);
    
?>