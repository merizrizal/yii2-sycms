<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use sycms\models\LinkItem;
use sycms\models\Page;
use sycms\models\ArticleCategory;
use sycms\models\ArticleItem;
use sycomponent\AjaxRequest;
use sycomponent\NotificationDialog;

/* @var $this yii\web\View */
/* @var $model sycms\models\LinkItem */
/* @var $form yii\widgets\ActiveForm */

kartik\select2\Select2Asset::register($this);
kartik\select2\ThemeKrajeeAsset::register($this);

$ajaxRequest = new AjaxRequest([
    'modelClass' => 'LinkItem',
]);

$ajaxRequest->form();

$status = Yii::$app->session->getFlash('status');
$message1 = Yii::$app->session->getFlash('message1');
$message2 = Yii::$app->session->getFlash('message2');

if ($status !== null) :
    $notif = new NotificationDialog([
        'status' => $status,
        'message1' => $message1,
        'message2' => $message2,
    ]);

    $notif->theScript();
    echo $notif->renderDialog();

endif; ?>

<?= $ajaxRequest->component() ?>

<div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-8">
        <div class="x_panel">
            <div class="link-item-form">

                <?php $form = ActiveForm::begin([
                        'id' => 'link-item-form',
                        'action' => $model->isNewRecord ? ['create', 'cid' => $model->link_category_id] : ['update', 'id' => $model->id],
                        'options' => [

                        ],
                        'fieldConfig' => [
                            'parts' => [
                                '{inputClass}' => 'col-lg-12'
                            ],
                            'template' => '<div class="row">'
                                            . '<div class="col-lg-3">'
                                                . '{label}'
                                            . '</div>'
                                            . '<div class="col-lg-6">'
                                                . '<div class="{inputClass}">'
                                                    . '{input}'
                                                . '</div>'
                                            . '</div>'
                                            . '<div class="col-lg-3">'
                                                . '{error}'
                                            . '</div>'
                                        . '</div>',
                        ]
                ]); ?>

                    <div class="x_title">

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6">
                                    <?php
                                    if (!$model->isNewRecord)
                                        echo Html::a('<i class="fa fa-upload"></i>&nbsp;&nbsp;&nbsp;' . 'Create', ['create', 'cid' => $model->link_category_id], ['class' => 'btn btn-success']); ?>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="x_content">

                        <?= $form->field($model, 'parent_id')->dropDownList(
                                ArrayHelper::map(
                                    LinkItem::find()->andWhere(['link_category_id' => $model->link_category_id])->orderBy('order')->asArray()->all(),
                                    'id',
                                    function($data) {
                                        return $data['title'];
                                    }
                                ),
                                [
                                    'prompt' => '',
                                    'style' => 'width: 100%'
                                ]) ?>

                        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'type')->dropDownList([ 'Page' => 'Halaman', 'Article-Category' => 'Kategori Artikel', 'Article-Item' => 'Artikel', 'Url' => 'Url', 'Anchor' => 'Anchor' ], ['prompt' => '']) ?>

                        <?= $form->field($model, 'page_id')->dropDownList(
                                ArrayHelper::map(
                                    Page::find()->orderBy('title')->asArray()->all(),
                                    'id',
                                    function($data) {
                                        return $data['title'];
                                    }
                                ),
                                [
                                    'prompt' => '',
                                    'style' => 'width: 100%'
                                ]) ?>

                        <?= $form->field($model, 'article_category_id')->dropDownList(
                                ArrayHelper::map(
                                    ArticleCategory::find()->orderBy('title')->asArray()->all(),
                                    'id',
                                    function($data) {
                                        return $data['title'];
                                    }
                                ),
                                [
                                    'prompt' => '',
                                    'style' => 'width: 100%'
                                ]) ?>

                        <?= $form->field($model, 'article_item_id')->dropDownList(
                                ArrayHelper::map(
                                    ArticleItem::find()->orderBy('title')->asArray()->all(),
                                    'id',
                                    function($data) {
                                        return $data['title'];
                                    }
                                ),
                                [
                                    'prompt' => '',
                                    'style' => 'width: 100%'
                                ]) ?>

                        <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'anchor')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'not_active')->checkbox(['value' => true], false) ?>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-6">
                                    <?php
                                    $icon = '<i class="fa fa-floppy-o"></i>&nbsp;&nbsp;&nbsp;';
                                    echo Html::submitButton($model->isNewRecord ? $icon . 'Save' : $icon . 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
                                    echo '&nbsp;&nbsp;&nbsp;';
                                    echo Html::a('<i class="fa fa-rotate-left"></i>&nbsp;&nbsp;&nbsp;Cancel', ['index', 'cid' => $model->link_category_id], ['class' => 'btn btn-default']); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
    <div class="col-sm-2"></div>
</div><!-- /.row -->

<?php

$this->registerCssFile($this->params['assetCommon']->baseUrl . '/plugins/iCheck/all.css', ['depends' => 'yii\web\YiiAsset']);

$this->registerJsFile($this->params['assetCommon']->baseUrl . '/plugins/iCheck/icheck.min.js', ['depends' => 'yii\web\YiiAsset']);

$jscript = '
    $("#linkitem-parent_id").select2({
        theme: "krajee",
        placeholder: "Pilih",
        allowClear: true
    });

    $("#linkitem-type").select2({
        theme: "krajee",
        placeholder: "Pilih",
        allowClear: true
    });

    $("#linkitem-page_id").select2({
        theme: "krajee",
        placeholder: "Pilih",
        allowClear: true
    });

    $("#linkitem-article_category_id").select2({
        theme: "krajee",
        placeholder: "Pilih",
        allowClear: true
    });

    $("#linkitem-article_item_id").select2({
        theme: "krajee",
        placeholder: "Pilih",
        allowClear: true
    });
    
    var hideLinkType = function(type) {
        $(".field-linkitem-page_id").hide();
        $(".field-linkitem-article_category_id").hide();
        $(".field-linkitem-article_item_id").hide();
        $(".field-linkitem-url").hide();
        $(".field-linkitem-anchor").hide();
        
        if (type != undefined) {
        
            type = type.toLowerCase();
            
            if (type != "url" && type != "anchor") {
                type = type.replace("-", "_") + "_id";
            }

            $(".field-linkitem-" + type).show();
        }
    };

    hideLinkType($("#linkitem-type").select2("data")[0].id);

    $("#linkitem-type").on("select2:select", function(e) {

        $("input#linkitem-page_id").val(null).trigger("change");
        $("input#linkitem-article_category_id").val(null).trigger("change");
        $("input#linkitem-article_item_id").val(null).trigger("change");
        $("input#linkitem-url").val(null);
        $("input#linkitem-anchor").val(null);
        
        hideLinkType($(this).select2("data")[0].id);                
    });

    $("#linkitem-type").on("select2:unselect", function(e) {

        $("input#linkitem-page_id").val(null).trigger("change");
        $("input#linkitem-article_category_id").val(null).trigger("change");
        $("input#linkitem-article_item_id").val(null).trigger("change");
        $("input#linkitem-url").val(null);
        $("input#linkitem-anchor").val(null);
        
        hideLinkType();
    });
';

$this->registerJs($jscript . Yii::$app->params['checkbox-radio-script']()); ?>