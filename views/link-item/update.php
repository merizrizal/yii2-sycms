<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sycms\models\LinkItem */

$this->title = 'Update Tautan: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Kategori Tautan', 'url' => ['link-category/index']];
$this->params['breadcrumbs'][] = ['label' => $model->linkCategory->title, 'url' => ['index', 'cid' => $model->link_category_id]];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="link-item-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
