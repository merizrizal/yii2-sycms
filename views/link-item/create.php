<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sycms\models\LinkItem */

$this->title = 'Create Tautan ' . $model->linkCategory->title;
$this->params['breadcrumbs'][] = ['label' => 'Kategori Tautan', 'url' => ['link-category/index']];
$this->params['breadcrumbs'][] = ['label' => $model->linkCategory->title, 'url' => ['index', 'cid' => $model->link_category_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="link-item-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
