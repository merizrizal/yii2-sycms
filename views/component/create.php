<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sycms\models\Component */

$this->title = 'Create Komponen';
$this->params['breadcrumbs'][] = ['label' => 'Komponen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="component-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
