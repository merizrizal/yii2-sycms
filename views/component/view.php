<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use sycomponent\AjaxRequest;
use sycomponent\ModalDialog;

/* @var $this yii\web\View */
/* @var $model sycms\models\Component */

$ajaxRequest = new AjaxRequest([
    'modelClass' => 'Component',
]);

$ajaxRequest->view();

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Komponen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title; ?>

<?= $ajaxRequest->component() ?>

<div class="component-view">

    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <div class="x_panel">

                <div class="x_content">

                    <?= Html::a('<i class="fa fa-pencil"></i>&nbsp;&nbsp;&nbsp;' . 'Edit',
                        ['update', 'id' => $model->id],
                        [
                            'class' => 'btn btn-primary',
                            'style' => 'color:white'
                        ]) ?>

                    <?= Html::a('<i class="fa fa-trash"></i>&nbsp;&nbsp;&nbsp;' . 'Delete',
                        ['delete', 'id' => $model->id],
                        [
                            'id' => 'delete',
                            'class' => 'btn btn-danger',
                            'style' => 'color:white',
                            'data-not-ajax' => 1,
                            'model-id' => $model->id,
                            'model-name' => $model->title,
                        ]) ?>

                    <?= Html::a('<i class="fa fa-rotate-left"></i>&nbsp;&nbsp;&nbsp;' . 'Cancel',
                        ['index'],
                        [
                            'class' => 'btn btn-default',
                        ]) ?>

                    <div class="clearfix" style="margin-top: 15px"></div>

                    <?= DetailView::widget([
                        'model' => $model,
                        'options' => [
                            'class' => 'table'
                        ],
                        'attributes' => [
                            'title',
                            'subtitle',
                            [
                                'attribute' => 'image',
                                'format' => 'raw',
                                'value' => Html::img(Yii::getAlias('@uploadsUrl') . $model->thumb('/img/component/', 'image', 200, 200), ['class'=>'img-thumbnail file-preview-image']),
                            ],
                            'text:ntext',
                            'slug',
                            [
                                'attribute' => 'not_publish',
                                'format' => 'raw',
                                'value' => Html::checkbox('not_publish[]', $model->not_publish, ['value' => $model->not_publish, 'disabled' => 'disabled']),
                            ],
                        ],
                    ]) ?>

                </div>

            </div>
        </div>
    </div>

</div>

<?php

$modalDialog = new ModalDialog([
    'clickedComponent' => 'a#delete',
    'modelAttributeId' => 'model-id',
    'modelAttributeName' => 'model-name',
]);

$modalDialog->theScript();

echo $modalDialog->renderDialog();

$this->registerCssFile($this->params['assetCommon']->baseUrl . '/plugins/iCheck/all.css', ['depends' => 'yii\web\YiiAsset']);

$this->registerJsFile($this->params['assetCommon']->baseUrl . '/plugins/iCheck/icheck.min.js', ['depends' => 'yii\web\YiiAsset']);

$jscript = Yii::$app->params['checkbox-radio-script']()
    . '$(".iCheck-helper").parent().removeClass("disabled");

    $("#table-article-item").find("th").css("width", "80px");
';

$this->registerJs($jscript);

?>